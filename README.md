# Tingg Checkout SDK

[<img src="https://cdn.cellulant.africa/images/brand-assets/tingg-by-cellulant-themed.svg" height="64">](https://tingg.africa)

### [Introduction](#introduction) | [Installation](#installation) | [License](#license)

# Introduction

Integrate your Php app with Tingg checkout in minutes using our feature rich & flexible SDK.

-   Express Checkout

# Installation

The SDK can be installed via composer using the command below:

```
composer require tingg/checkout
```

# Express checkout integration

> Ensure you get your client id and client secret keys to continue. [See docs](https://docs.tingg.africa/docs/checkout-v3-getting-started#4--checkout-api-keys)

Let's look at a simple Express APP route that creates a checkout request

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tingg\Checkout\Express;

class CheckoutController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        # Get these values from your .env config   
        $client_secret =env('clientSecret');
        $client_id = env('clientId');
        $environment = "testing";

        // Get the JSON payload from the request
        $payload = $request->json()->all();
        
        // A sample for $request->json()->all() returned value
        // $payload = [
        //     "merchant_transaction_id" => "mtr-dg9823euy3a",
        //     "account_number" => "acc-14n345j5599",
        //     "msisdn" => "254700000000",
        //     "service_code" => "JOHNDOEONLINE",
        //     "country_code" => "KEN",
        //     "currency_code" => "KES",
        //     "customer_last_name" => "John",
        //     "customer_first_name" => "Doe",
        //     "customer_email" => "tingg@cellulant.io",
        //     "request_amount" => "100",
        //     "due_date" => "2023-11-18 16:15:30",
        //     "language_code" => "en",
        //     "request_description" => "Dummy merchant transaction",
        //     "fail_redirect_url" => "https://webhook.site/88390df9-a496-432f-abe5-0cf3380fda54",
        //     "success_redirect_url" => "https://webhook.site/88390df9-a496-432f-abe5-0cf3380fda54",
        //     "callback_url" => "https://webhook.site/88390df9-a496-432f-abe5-0cf3380fda54",
        // ];
        
        // Init a checkout request
        $express = new Express();
        
        // validates & encrypt the request
        $data = $express->create($payload,$environment,$client_id,$client_secret);
        // Handle errors if error key is not empty
        // 1. Returns validation & API errors if any i.e $result['error']['message']

        return response()->json([
            "long_url" => $data['results']['long_url'],
            "short_url" => $data['results']['short_url']
        ]);
    }
}
```

# License

This SDK is distributed under the
[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0),
see [LICENSE.txt](./LICENSE.txt) for more information.
