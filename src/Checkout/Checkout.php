<?php

namespace Tingg\Checkout;

use Tingg\Checkout\Constants as Constants;
use Tingg\Checkout\Validation as Validation;
use Tingg\Checkout\Encryption as Encryption;

class Checkout
{
    private $encryption;
    public function __construct($iv_key, $secret_key) {
        $this->encryption = new Encryption($iv_key, $secret_key);
    }

    public function express($payload, $access_key, $environment) {
        try {
            $validation = (new Validation($payload))->validate();

            // return validation errors
            if (!isset($validation['data'])) return ["error" => $validation['error']];

            // return access_key error
            if (!(!empty($access_key) && is_string($access_key)))
                return ["error" => ["message" => "The access_key should be a valid none-empty string"]];

            // return environment errors
            if (!in_array($environment, Constants::SUPPORTED_ENVIRONMENTS, true))
                return ["error" => ["message" => "The environment should be one of " . implode(", ", Constants::SUPPORTED_ENVIRONMENTS)]];

            $encrypted_params = $this->encryption->encrypt($validation['data']);

            return [
                "data" => [
                    "access_key" => $access_key,
                    "encrypted_params" => $encrypted_params
                ]
            ];
        } catch (\Exception $ex) {
            var_dump($ex->getTraceAsString());

            return [
                "error" => ["message" => "Sorry, unable to create a checkout request"]
            ];
        }
    }
}