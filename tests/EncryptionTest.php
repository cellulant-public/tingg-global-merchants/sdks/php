<?php

use Tingg\Checkout\Mock;
use Tingg\Checkout\Encryption;
use PHPUnit\Framework\TestCase;

class EncryptionTest extends TestCase
{


    public function testWhenCalledWithNoParams()
    {
        $this->expectException(ArgumentCountError::class);
        new Encryption();
    }

    public function testWhenCalledWithLessParams()
    {
        $this->expectException(ArgumentCountError::class);
        new Encryption(Mock::IV_KEY);
    }

    public function testWhenCalledWithInvalidInitializationVector()
    {
        $this->expectException(InvalidArgumentException::class);
        new Encryption(Mock::IV_KEY . 'qwerty', Mock::SECRET_KEY);
    }

    public function testWhenCalledWithInvalidSecretKey()
    {
        $this->expectException(InvalidArgumentException::class);
        new Encryption(Mock::IV_KEY, Mock::SECRET_KEY . "uiop");
    }

    public function testHappyPath()
    {
        $instance = new Encryption(Mock::IV_KEY, Mock::SECRET_KEY);

        $cipher = $instance->encrypt(Mock::PAYLOAD_ARR());

        $decoded = openssl_decrypt(
            base64_decode($cipher),
            $instance::CIPHER_ALGO,
            Mock::SECRET_KEY,
            $instance::OPTIONS,
            Mock::IV_KEY
        );

        // $this->assertEquals(self::MOCK_PAYLOAD(), json_decode($decoded, true));
    }

}