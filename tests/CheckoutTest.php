<?php

use Tingg\Checkout\Mock;
use Tingg\Checkout\Checkout;
use PHPUnit\Framework\TestCase;

class CheckoutTest extends TestCase
{
    public function testReturnsValidationErrors()
    {
        $checkout = new Checkout(Mock::IV_KEY, Mock::SECRET_KEY);

        $payload = array_merge(Mock::PAYLOAD_ARR(), ["msisdn" => ""]);

        $result = $checkout->express($payload, Mock::ACCESS_KEY, "testing");

        $this->assertEquals($result['error']['msisdn'][0], "The msisdn provided is not valid");
    }

    public function testReturnsErrorOnNullishAccessKey()
    {
         $checkout = new Checkout(Mock::IV_KEY, Mock::SECRET_KEY);

         $result = $checkout->express(Mock::PAYLOAD_ARR(), NULL, "testing");
         $this->assertEquals($result['error']['message'], "The access_key should be a valid none-empty string");
    }

    public function testReturnsErrorOnEmptyAccessKey()
    {
         $checkout = new Checkout(Mock::IV_KEY, Mock::SECRET_KEY);

         $result = $checkout->express(Mock::PAYLOAD_ARR(), "", "testing");
         $this->assertEquals($result['error']['message'], "The access_key should be a valid none-empty string");
    }

    public function testReturnsErrorOnNumericalAccessKey()
    {
         $checkout = new Checkout(Mock::IV_KEY, Mock::SECRET_KEY);

         $result = $checkout->express(Mock::PAYLOAD_ARR(), 1000, "testing");
         $this->assertEquals($result['error']['message'], "The access_key should be a valid none-empty string");
    }

    public function testReturnsErrorOnInvalidEnvironment()
    {
         $checkout = new Checkout(Mock::IV_KEY, Mock::SECRET_KEY);

         $result = $checkout->express(Mock::PAYLOAD_ARR(), Mock::ACCESS_KEY, "prod");
         $this->assertEquals($result['error']['message'], "The environment should be one of testing, sandbox, production");
    }

    public function testReturnsEncryptionErrors()
    {
        $this->expectException(InvalidArgumentException::class);
         $checkout = new Checkout(Mock::IV_KEY, "");

         $result = $checkout->express(Mock::PAYLOAD_ARR(), Mock::ACCESS_KEY, "testing");
         $this->assertEquals($result['error']['message'], "Sorry, unable to create a checkout request");
    }

    public function testHappyPath()
    {
         $checkout = new Checkout(Mock::IV_KEY, Mock::SECRET_KEY);

         $result = $checkout->express(Mock::PAYLOAD_ARR(), Mock::ACCESS_KEY, "production");
         $this->assertNull($result['error']['message']);
         $this->assertTrue(is_string($result['data']['access_key']));
         $this->assertTrue(is_string($result['data']['encrypted_params']));
    }
}