<?php


use Tingg\Checkout\Mock;
use Tingg\Checkout\Express;
use PHPUnit\Framework\TestCase;

class ExpressTest extends TestCase
{

    public function testReturnsErrorOnNullishClientId()
    {
        $express = new Express();

        $result = $express->create(Mock::PAYLOAD_ARR(), "testing", null, Mock::CLIENT_SECRET);

        $this->assertEquals("ClientId should not be empty.", $result['error']['message']);
    }

    public function testReturnsErrorOnNullishClientSecret()
    {
        $express = new Express();

        $result = $express->create(Mock::PAYLOAD_ARR(), "testing", Mock::CLIENT_ID, null);

        $this->assertEquals("Client Secret should not be empty.", $result['error']['message'],);
    }

    public function testReturnsErrorOnInvalidEnvironment()
    {
        $express = new Express();

        $result = $express->create(Mock::PAYLOAD_ARR(), "prod", Mock::CLIENT_ID, Mock::CLIENT_SECRET);

        $this->assertEquals("The environment should be one of testing, sandbox, production", $result['error']['message']);
    }


    public function testReturnsErrorOnInvalidClientId()
    {
        $express = new Express();

        $result = $express->create(Mock::PAYLOAD_ARR(), "testing", "test", Mock::CLIENT_SECRET);

        $this->assertEquals("Authentication failed. Check your credentials, and try again.", $result['error']['message']);
    }

    public function testReturnsErrorOnInvalidClientSecret()
    {
        $express = new Express();

        $result = $express->create(Mock::PAYLOAD_ARR(), "testing", Mock::CLIENT_ID, "invalidSecret");

        $this->assertEquals("Authentication failed. Check your credentials, and try again.", $result['error']['message']);
    }

    public function testReturnsErrorOnInvalidPayload()
    {
        $express = new Express();

        $result = $express->create("invalidPayload", "testing", Mock::CLIENT_ID, "invalidSecret");

        $this->assertEquals("Invalid payload. JSON string or associative array expected", $result['error']['message']);
    }

    public function testExpressCheckoutRequest()
    {
        $express = new Express();
        $result = $express->create(Mock::PAYLOAD_ARR(), "testing", Mock::CLIENT_ID, Mock::CLIENT_SECRET);
        if (isset($result["data"]["status"]["status_description"])) {
            $this->assertEquals("success", $result["data"]["status"]["status_description"]);
        } else {
            $this->assertEquals("No Response from express checkout.", $result['error']['message']);
        }
    }

}